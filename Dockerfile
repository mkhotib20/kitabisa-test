FROM node:14.17.0-alpine

WORKDIR /apps

COPY package.json pnpm-lock.yaml ./

RUN npm i -g pnpm@6 pm2
RUN pnpm install

COPY . .

RUN pnpm run build:all

EXPOSE 3500
CMD [ "pm2-runtime", "start", "npm -- start" ]
