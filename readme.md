## Kitabisa Test
### This project is used to display a campaign of kita bisa, with following features

-  **Order by remaining days :** You can order the data with remaining days, ascending or descending
-  **Order by donation target :** You can order the data with donation target, ascending or descending
- **Server side rendered :** This project is SEO friendly because server side rendered, with initial props that fetched in server while user request

#### [DEMO](https://khotib-porto.rebaseproject.com)

### Overall Project decision explanation
1. why i use `pnpm`, instead of other package manager, ?, it's about speed and disk efficiency, [Reference](https://github.com/pnpm/benchmarks-of-javascript-package-managers), pnpm is the fastest even without lock file. Pnpm uses symlinks to add only the direct dependencies of the project into the root of the modules directory. [Reference](https://pnpm.io/motivation)
2. Why i use `@loadable/component` ? , i need to split the chunk file in an easy way. The JS file will be splitted into separate `.js` file based on route or component, this way can reduce total blocking time. you can interact with other component while waiting other huge size chunk file loaded.
3. Why ssr ?, it's needed to make the web crawled by google/other search engine bot (SEO friendly), as far as i know, google bot crawl the site in plain html without browser to compile your `JS` code, if your site rendered in client after the `js` excecuted by the browser, i think google bot only get empty html, which means yoru site is empty and nothing to crawl. Beside that, it can reduce `Commulative layout shift` and `First contentful paint`, because when first render the content already painted and should'nt shifted.

4. Inside `src/client/hooks/useAxios/index.ts` you can see, i use `use-sse` hooks, this is aimed to make the axios hooks run in server side while generating html pages, so that the server can fetch the data needed and render the html
5. Why `@motion/react` instead of `css` ?, emotion has good `specificity level`, your style will specified only for the components and make your code modular. you can reuse the component anywhere without mixing style with other components. and the className will be generated by emotion, so the style will not mixed with other className that created by our self inside css file. And yes you can pass variable from component, state, or other variable that affect the styling. you can literally do javascript calculation with the style.

6. Custom axios hooks, i create the hooks to make api call easy, this hooks can be reused in other component that `need api call, data state, loading state and error state`.  so you will not repeatedly create api call, useEffect, data state, loading state and error state whenever you need api call
7. Infinite scroll pagination, you can see, if you have reach the bottom of content the browser will request for the next page, and so on.
i prefer calculate scroll height and container height to decide either it has been reach bottom or not. In other way you can use Intersection Observer, but this way will not works in old version browser. 

 
### How to run in *Development mode*
#### <strong> please take a note, this project crafted with pnpm, so the module version locked with pnpm-lock.yml. To prevent unexpected error because different library version you've installed, i recommend to use pnpm or import the lock file to your prefered package manager 
</strong>

 1. Clone the repo

```console
git clone https://gitlab.com/mkhotib20/kitabisa-test.git
 ```
 2. Copy `.env.example` as `.env`, and input your prefered config
 ```console
cp .env.example .env
 ```
 3. Import dependency lock file to your prefered package manager (***if you're using pnpm, skip this step***), please use [Synp](https://github.com/imsnif/synp) or other library  to do it.
 4. Install dependencies

```console
pnpm install
or
yarn install
or
npm install
 ```

 5. Run server  (this server used to run fake backend from json datasource, so that we can sort and manipulate the data inside server code)
```console
pnpm run dev:server
```
 6. Run client 
```console
pnpm run dev:client
```
7. A new tab will be automatically opened in your browser, the apps running on PORT that you've set before on `.env` file




### How to run in *Production mode (docker compose)*

#### <strong> i recommend to use docker for production mode, it's nearly zero config, you just need to copy the `docker-compose.yml`  </strong>

1. Create `docker-compose.yml` , you can copy from this repo and complete the environtment variables based on your prefered config
2. Run the docker
```console
sudo docker-compose up -d
```
3. You can see, he apps running on PORT that you've set before inside `docker-compose.yml` file

### Available scripts 

```console
pnpm run build:client // to build client code inside src/client
pnpm run build:server // to build client code inside src/server
pnpm run dev:client // run client locally
pnpm run dev:server // run server locally
pnpm run build:all // build both client and server
pnpm run start // start in production mode
pnpm run test // run unit test
pnpm run test -- --coverage // run unit test with coverage report
```

### Unit test coverage result
![UT Coverage result](https://gitlab.com/mkhotib20/kitabisa-test/-/raw/master/screenshoots/code-coverage.png)

### Lighthouse audit result
![Lighthouse audit result](https://gitlab.com/mkhotib20/kitabisa-test/-/raw/master/screenshoots/audit-result.png)