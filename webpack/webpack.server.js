const nodeExternals = require('webpack-node-externals');

const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const sharedConfig = require('./webpack.shared');

module.exports = {
  ...sharedConfig,
  entry: './src/server/index.ts',
  target: ['node', 'es5'],
  output: {
    path: path.resolve(__dirname, '../dist/server'),
    filename: '[name].js',
  },

  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: 'views/server.html',
          to: 'renderer/html-template.html',
        },
        {
          from: 'public',
          to: '../client',
        },
      ],
    }),
    ...sharedConfig.plugins,
  ],
  externals: [nodeExternals()],
};
