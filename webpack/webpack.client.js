const LoadablePlugin = require('@loadable/webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const { HotModuleReplacementPlugin } = require('webpack');
const sharedConfig = require('./webpack.shared');

module.exports = {
  ...sharedConfig,
  entry: './src/client/index.tsx',
  target: 'browserslist',
  output: {
    publicPath: '/',
    path: path.resolve(__dirname, '../dist/client'),
    filename: '[name].[contenthash].js',
  },
  plugins: [
    new LoadablePlugin(),
    ...(process.env.NODE_ENV !== 'production'
      ? [
          new HtmlWebpackPlugin({
            template: 'views/client.html',
          }),
        ]
      : []),
    new HotModuleReplacementPlugin(),
    ...sharedConfig.plugins,
  ],

  devServer: {
    static: path.join(__dirname, '../dist/client'),
    historyApiFallback: true,
    port: process.env.PORT || 3500,
    open: true,
    hot: true,
  },
  performance: {
    hints: false,
  },
};
