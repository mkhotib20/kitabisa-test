import loadable from '@loadable/component';

const DonasiCard = loadable(() => import(/* webpackChunkName: "donasi-card" */ './view'));

export default DonasiCard;
