import { css } from '@emotion/react';

export const cssDonasiCardWrapper = css({
  borderRadius: 10,
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
  boxShadow: '0px 0px 4px #DBDBDB',
});

export const cssDonationTitle = css({
  color: '#555',
  marginBottom: 20,
  display: '-webkit-box',
  WebkitLineClamp: 2,
  WebkitBoxOrient: 'vertical',
  height: 55,
  fontSize: `${16 / 14}rem`,
  overflow: 'hidden',
  textDecoration: 'unset',
});

export const cssCardImage = css({
  height: 200,
  width: '100%',
  objectFit: 'cover',
});

export const cssDonationPercentageWrapper = css({
  height: 8,
  backgroundColor: '#dbdbdb',
  borderRadius: 8,
  overflow: 'hidden',
});

export const cssDonationPercentLine = (percent: number) =>
  css({
    height: '100%',
    backgroundColor: percent >= 100 ? 'pink' : 'gray',
    width: `${percent}%`,
  });

export const cssCampaignerName = css({
  display: 'flex',
  alignItems: 'center',
  '>span': {
    marginTop: 0,
    display: '-webkit-box',
    WebkitLineClamp: 1,
    WebkitBoxOrient: 'vertical',
    overflow: 'hidden',
    fontSize: `${12 / 14}rem`,
  },
  '>img': {
    marginRight: 8,
  },
  paddingBottom: 10,
});

export const cssCardFooter = css({
  flex: 1,
  fontSize: 10,
  marginTop: 8,
  height: 35,
  '>p': {
    margin: 0,
    textAlign: 'center',
  },
  'p:first-of-type': {
    fontWeight: 500,
  },
  'p:last-child': {
    fontSize: 14,
  },
});

export const cssCardFooterContainer = css({
  display: 'flex',
  alignItems: 'center',
  margin: '20px 0',
});
