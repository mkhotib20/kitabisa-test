import { Donasi } from '@/types/donasi';
import type { VFC } from 'react';
import {
  cssCampaignerName,
  cssCardFooter,
  cssCardFooterContainer,
  cssCardImage,
  cssDonasiCardWrapper,
  cssDonationPercentageWrapper,
  cssDonationPercentLine,
  cssDonationTitle,
} from './styles';
import userIcon from './assets/user.svg';
import targetIcon from './assets/target.svg';

interface ViewDonasiCardProps extends Donasi {}

const ViewDonasiCard: VFC<ViewDonasiCardProps> = ({
  image,
  title,
  campaigner,
  donation_received,
  donation_percentage,
  days_remaining,
  donation_target,
}) => {
  return (
    <div css={cssDonasiCardWrapper}>
      <img src={image} alt={title} css={cssCardImage} height={200} width={300} />
      <div style={{ padding: '10px 20px' }}>
        <a href="https://kitabisa.com" css={cssDonationTitle}>
          {title}
        </a>
        <div css={cssCampaignerName}>
          <img height={16} width={16} src={userIcon} alt="" /> <span title={campaigner}>{campaigner}</span>
        </div>
        <div css={[cssCampaignerName, { marginBottom: 10 }]}>
          <img height={16} width={16} src={targetIcon} alt="" /> <span>{donation_target.toLocaleString()}</span>
        </div>
        <div css={cssDonationPercentageWrapper}>
          <div css={cssDonationPercentLine(donation_percentage * 100)} />
        </div>
        <div css={cssCardFooterContainer}>
          <div css={cssCardFooter}>
            <p>Terkumpul</p>
            <p>Rp {donation_received.toLocaleString()}</p>
          </div>
          <div css={cssCardFooter}>
            <p>Sisa Hari</p>
            <p>{days_remaining > 0 ? `${days_remaining.toLocaleString()} hari` : 'Telah selesai'}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewDonasiCard;
