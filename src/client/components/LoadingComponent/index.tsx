import loadable from '@loadable/component';

const Loading = loadable(() => import(/* webpackChunkName: "loading-component" */ './view'));

export default Loading;
