import loadingImg from './assets/loading.svg';

const ViewLoading = () => (
  <div css={{ textAlign: 'center', margin: '20px auto' }}>
    <img alt="" src={loadingImg} width={50} height={50} />
  </div>
);

export default ViewLoading;
