import React from 'react';
import { hydrate } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { loadableReady } from '@loadable/component';
import { createBroswerContext } from 'use-sse';

const BroswerDataContext = createBroswerContext();

loadableReady(() => {
  hydrate(
    <React.StrictMode>
      <BroswerDataContext>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </BroswerDataContext>
    </React.StrictMode>,
    document.getElementById('root')
  );
});
