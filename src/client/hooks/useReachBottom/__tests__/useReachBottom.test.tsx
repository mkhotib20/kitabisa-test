import * as envCheck from '@/utils/canUseDOM';
import renderHookWrapper from '@/utils/test/renderWrapper/hook';
import '@testing-library/jest-dom';
import { fireEvent } from '@testing-library/react';
import useReachBottom from '../index';

const mockCallback = jest.fn();

describe('useReachBottom', () => {
  const originalEnv = envCheck.default;
  afterEach(() => {
    jest.resetAllMocks();
    // @ts-ignore
    envCheck.default = originalEnv;
  });

  it('Should render hooks and refetch correctly', async () => {
    Object.defineProperty(document.body, 'clientHeight', {
      value: 2500,
    });
    const { result } = renderHookWrapper(() => useReachBottom(mockCallback));

    // mock client height
    // @ts-ignore
    result.current.current = {
      clientHeight: 2000,
    };

    fireEvent.scroll(window, {
      target: { scrollY: 100 },
    });
    expect(mockCallback).not.toBeCalled();

    // Mock user scroll untill botttom
    fireEvent.scroll(window, {
      target: { scrollY: 2000 },
    });
    // the callback should be fired
    expect(mockCallback).toBeCalled();
  });
});
