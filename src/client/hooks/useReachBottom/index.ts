import { useEffect, useRef } from 'react';
import type { DependencyList } from 'react';

const useReachBottom = (callback: () => void, deps?: DependencyList) => {
  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleOnScroll = () => {
      const donasiContainerHeight = containerRef.current.clientHeight;

      const donasiContainerOffsetTop = document.body.clientHeight - containerRef.current.clientHeight;
      const scrollHeight = window.innerHeight + window.scrollY - donasiContainerOffsetTop;

      const isBottomReached = Math.ceil(scrollHeight) >= donasiContainerHeight;
      if (isBottomReached) callback();
    };
    window.addEventListener('scroll', handleOnScroll);
    return () => {
      window.removeEventListener('scroll', handleOnScroll);
    };
  }, deps);

  return containerRef;
};

export default useReachBottom;
