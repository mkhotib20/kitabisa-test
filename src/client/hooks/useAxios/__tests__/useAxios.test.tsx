import * as envCheck from '@/utils/canUseDOM';
import renderHookWrapper from '@/utils/test/renderWrapper/hook';
import '@testing-library/jest-dom';
import { act } from 'react-dom/test-utils';
import useAxios from '../index';
import { paginationFlowDataMocks, refetchFlowDataMocks } from '../__data_mocks__/axiosMockData';

describe('useAxios', () => {
  const originalEnv = envCheck.default;
  afterEach(() => {
    jest.resetAllMocks();
    // @ts-ignore
    envCheck.default = originalEnv;
  });

  it('Should render hooks and refetch correctly', async () => {
    const { result } = renderHookWrapper(() => useAxios<{ hello: string }>('https://test.url.com'), {
      apiMocks: refetchFlowDataMocks,
    });
    await act(async () => {
      await result.current.refetch();
    });

    await act(async () => {
      await result.current.refetch({
        name: 'khotib',
      });
    });
    expect(result.current.data.hello).toEqual('There');
  });

  it('Should render pagination correctly', async () => {
    const { result } = renderHookWrapper(() => useAxios<{ hello: string[] }>('https://test.url.com'), {
      apiMocks: paginationFlowDataMocks,
    });
    await act(async () => {
      await result.current.refetch();
    });

    expect(result.current.data.hello).toHaveLength(3);

    await act(async () => {
      await result.current.fetchMore({
        variables: {
          page: '2',
        },
        updateData: (prev, newResult) => ({
          hello: [...prev.hello, ...newResult.hello],
        }),
      });
    });
    expect(result.current.data.hello).toHaveLength(6);
  });

  // non infinite scroll, replace old data with the new one
  it('Should render pagination with loading state correctly', async () => {
    // mock ssr
    // @ts-ignore
    envCheck.default = false;
    const { result } = renderHookWrapper(() => useAxios<{ hello: string[] }>('https://test.url.com'), {
      apiMocks: paginationFlowDataMocks,
    });
    await act(async () => {
      await result.current.refetch();
    });

    expect(result.current.data.hello).toHaveLength(3);

    await act(async () => {
      result.current.fetchMore({
        variables: {
          page: '2',
        },
        notifyLoading: true,
        updateData: (_, newResult) => ({
          hello: newResult.hello,
        }),
      });
    });
    expect(result.current.data.hello).toHaveLength(3);
  });

  // non infinite scroll, replace old data with the new one
  it('Should render hooks with error', async () => {
    const { result } = renderHookWrapper(() => useAxios<{ hello: string[] }>('https://test.url.com'), {});
    await act(async () => {
      await result.current.refetch();
    });

    expect(result.current.data).toBeNull();

    await act(async () => {
      result.current.fetchMore({
        variables: {
          page: '2',
        },
        notifyLoading: true,
        updateData: (_, newResult) => ({
          hello: newResult.hello,
        }),
      });
    });

    expect(result.error).not.toBeNull();
  });
});
