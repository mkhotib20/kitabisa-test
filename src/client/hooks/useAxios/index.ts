import canUseDOM from '@/utils/canUseDOM';
import axios from 'axios';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useSSE } from 'use-sse';

type VariablesType = Record<string, string>;

interface FetcherOption<DT> {
  variables?: VariablesType;
  notifyLoading?: boolean;
  updateData: (prevResult: DT, fetchMoreResult: DT) => DT;
}
export type FetchmoreFuncType<DT> = (option: FetcherOption<DT>) => Promise<DT>;
export type FetcherFuncType<DT> = (variables?: VariablesType) => Promise<DT>;

const useAxios = <DT>(url: string) => {
  const prevData = useRef<DT>();
  const initialLoading = useMemo(() => {
    if (canUseDOM) {
      return !window._initialDataContext;
    }
    return false;
  }, []);

  const [loading, setLoading] = useState(initialLoading);

  const doFetch: FetcherFuncType<DT> = useCallback(
    async (variables = {}) => {
      try {
        const varKeys = Object.keys(variables);
        const searchParams = new URLSearchParams();
        varKeys.forEach((key) => {
          searchParams.append(key, variables[key]);
        });
        const { data } = await axios.get<DT>(`${url}${varKeys.length ? `?${searchParams.toString()}` : ''}`);

        return data;
      } catch (error) {
        throw error;
      } finally {
        setLoading(false);
      }
    },
    [url]
  );

  const [serverData, serverError] = useSSE(() => {
    return doFetch();
  }, [doFetch]);

  const [data, setData] = useState<DT>(serverData);
  const [error, setError] = useState(serverError);

  const refetch: FetcherFuncType<DT> = useCallback(
    async (variables = {}) => {
      try {
        setLoading(true);
        const data = await doFetch(variables);
        setData(data);
        return data;
      } catch (error) {
        setError(error);
        return error;
      } finally {
        setLoading(false);
      }
    },
    [doFetch]
  );

  const fetchMore: FetchmoreFuncType<DT> = useCallback(
    async (option) => {
      const { variables, updateData, notifyLoading } = option;
      try {
        if (notifyLoading) setLoading(true);
        const newResult = await doFetch(variables);
        const newData = updateData(prevData.current, newResult);
        setData(newData);
        return data;
      } catch (error) {
        setError(error);
        console.error(error);
        return error;
      } finally {
        if (notifyLoading) setLoading(false);
      }
    },
    [doFetch]
  );
  useEffect(() => {
    prevData.current = data;
  }, [data]);

  useEffect(() => {
    setData(serverData);
  }, [serverData]);

  return {
    data,
    error,
    loading,
    refetch,
    fetchMore,
  };
};

export default useAxios;
