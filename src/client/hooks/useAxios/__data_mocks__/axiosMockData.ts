import { ApiMockTypes } from '@/utils/test/renderWrapper/const';

export const refetchFlowDataMocks: ApiMockTypes[] = [
  {
    url: 'https://test.url.com',
    data: {
      hello: 'World',
    },
    method: 'get',
  },
  {
    url: 'https://test.url.com?name=khotib',
    data: {
      hello: 'There',
    },
    method: 'get',
  },
  {
    url: 'https://test.url.com?page=2',
    data: {
      hello: 'There',
    },
    method: 'get',
  },
];

export const paginationFlowDataMocks: ApiMockTypes[] = [
  {
    url: 'https://test.url.com',
    data: {
      hello: ['Pertama', 'Kedua', 'Ketiga'],
    },
    method: 'get',
  },
  {
    url: 'https://test.url.com?page=2',
    data: {
      hello: ['Keempat', 'Kelima', 'Keenam'],
    },
    method: 'get',
  },
];
