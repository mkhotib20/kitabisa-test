import React from 'react';

import { Global, css } from '@emotion/react';

export const globalStyles = (
  <Global
    styles={css`
      body,
      html {
        margin: 0;
      }
      a,
      p,
      span,
      h3 {
        font-family: 'Poppins', sans-serif;
      }
    `}
  />
);
