import { renderHook } from '@testing-library/react-hooks';
import { beforeRender } from '.';
import { defaultOption } from './const';

const renderHookWrapper = <TProps, TResult>(hook: () => TResult, options = defaultOption) => {
  const { apiMocks } = options;
  beforeRender({ apiMocks });
  return renderHook<TProps, TResult>(hook);
};

export default renderHookWrapper;
