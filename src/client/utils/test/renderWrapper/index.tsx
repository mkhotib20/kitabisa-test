import { render } from '@testing-library/react';
import axios from 'axios';
import type { ReactNode } from 'react';
import { HelmetProvider } from 'react-helmet-async';
import { MemoryRouter } from 'react-router-dom';
import { createBroswerContext } from 'use-sse';
import { defaultOption } from './const';

jest.mock('axios');

export const beforeRender = (options = defaultOption) => {
  const { apiMocks } = options;

  // In case method post, create another one
  const mockAxios = jest.spyOn(axios, 'get');
  mockAxios.mockImplementation(async (url) => {
    let mockData = null;

    if (apiMocks) {
      const mockBasedOnUrl = apiMocks.find((item) => item.url === decodeURIComponent(url));
      mockData = mockBasedOnUrl?.data;
      return Promise.resolve({ data: mockData });
    } else {
      return Promise.reject({
        error: 'Test error',
      });
    }
  });
};

const renderWrapper = (component: ReactNode, options = defaultOption) => {
  const { apiMocks, ...restOptions } = options;
  const BrowserContext = createBroswerContext();
  beforeRender({ apiMocks });

  render(
    <BrowserContext>
      <HelmetProvider>
        <MemoryRouter {...restOptions}>{component}</MemoryRouter>
      </HelmetProvider>
    </BrowserContext>
  );
};

export default renderWrapper;
