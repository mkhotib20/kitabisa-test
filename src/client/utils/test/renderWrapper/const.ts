import type { MemoryRouterProps } from "react-router";

export interface ApiMockTypes {
  url: string;
  method: 'post' | 'get';
  data: any;
  ssr?: boolean;
}

export interface RenderWrapperOption extends MemoryRouterProps {
  apiMocks?: Array<ApiMockTypes>;
}

export const defaultOption: RenderWrapperOption = {
  initialEntries: ['/'],
};
