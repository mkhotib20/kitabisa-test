import '@testing-library/jest-dom';

describe('Can Use DOM in SSR', () => {
  const originalWindowObject = global.window;
  afterEach(() => {
    Object.defineProperty(global, 'window', {
      value: originalWindowObject,
    });
  });

  it('Should return false', () => {
    Object.defineProperty(global, 'window', {
      value: undefined,
    });
    const canUseDOM = require('../index').default;
    expect(canUseDOM).toBe(false);
  });
});
