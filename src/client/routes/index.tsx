import loadable from '@loadable/component';

const Routes = loadable(() => import(/* webpackChunkName: "main-router" */ './routes'), {
  ssr: true,
});

export default Routes;
