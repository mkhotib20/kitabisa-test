import type { ChangeEventHandler, VFC } from 'react';
import { FetcherFuncType } from '@/hooks/useAxios';
import { cssFilterWrapper, cssMainIcon, cssSelectFilter } from '../styles';
import iconKb from '../assets/icon.png';
interface FilterPropTypes {
  onFilter: (variables: Record<string, string>) => void;
}

const Filter: VFC<FilterPropTypes> = ({ onFilter }) => {
  const handleChangeOrder: ChangeEventHandler<HTMLSelectElement> = (event) => {
    onFilter({
      ...(event.currentTarget.value && { order: event.currentTarget.value }),
    });
  };

  return (
    <div css={cssFilterWrapper}>
      <div css={{ flex: 1 }}>
        <img width={243} height={50} css={cssMainIcon} src={iconKb} alt="Kita bisa" />
      </div>
      <select css={cssSelectFilter} data-testid="selectFilter" onChange={handleChangeOrder}>
        <option value="">Urutan Default</option>
        <option value="donation_target,asc">Donation Goal terendah</option>
        <option value="donation_target,desc">Donation Goal tertinggi</option>
        <option value="days_remaining,asc">Sisa Hari paling sebentar</option>
        <option value="days_remaining,desc">Sisa Hari paling lama</option>
      </select>
    </div>
  );
};

export default Filter;
