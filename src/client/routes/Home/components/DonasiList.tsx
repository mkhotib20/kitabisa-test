import DonasiCard from '@/components/DonasiCard';
import Loading from '@/components/LoadingComponent';
import useReachBottom from '@/hooks/useReachBottom';
import { Donasi } from '@/types/donasi';
import { VFC } from 'react';
import { ApiBaseResponse } from '../interface';
import { cssDonasiItemWrapper, cssDonasiWrapper } from '../styles';

interface DonasiListPropTypes {
  loading: boolean;
  response: ApiBaseResponse<Donasi[]>;
  onNextPage: () => void;
}

const DonasiList: VFC<DonasiListPropTypes> = ({ loading, response, onNextPage }) => {
  const { hasNext } = response || {};

  const containerRef = useReachBottom(() => {
    if (!loading && hasNext) onNextPage();
  }, [loading, hasNext, onNextPage]);

  if (loading) return <Loading />;

  if (!response?.data) {
    return <p>Something went wrong, please try again later </p>;
  }

  const dataDonasi = response.data;

  return (
    <div ref={containerRef} css={cssDonasiWrapper}>
      {dataDonasi.map((donasi) => (
        <div key={donasi.id} css={cssDonasiItemWrapper}>
          <DonasiCard {...donasi} />
        </div>
      ))}
      {hasNext && <Loading />}
    </div>
  );
};

export default DonasiList;
