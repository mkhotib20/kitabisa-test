import renderWrapper from '@/utils/test/renderWrapper';
import '@testing-library/jest-dom';
import { fireEvent, screen } from '@testing-library/react';
import Home from '../index';
import { apiMocks } from '../__data_mocks__/mockCampaignData';

jest.mock('axios');

describe('Home Page', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('Should render correctly with data', async () => {
    renderWrapper(<Home />, {
      apiMocks,
    });

    // Ensure page loaded successfully
    expect(await screen.findByText(/Kampanye urutan default/i)).toBeInTheDocument();
    const selectFilter = screen.getByTestId('selectFilter');
    fireEvent.change(selectFilter, { target: { value: 'donation_target,asc' } });
    expect(await screen.findByText(/Kampanye paling sedikit/i)).toBeInTheDocument();

    // Reset order
    fireEvent.change(selectFilter, { target: { value: '' } });
    expect(await screen.findByText(/Kampanye urutan default/i)).toBeInTheDocument();
    fireEvent.scroll(window);
  });

  it('Should render correctly with error and show error', async () => {
    renderWrapper(<Home />);
    expect(await screen.findByText(/Something went wrong, please try again later/i)).toBeInTheDocument();
  });
});
