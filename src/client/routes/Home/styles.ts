import { css } from '@emotion/react';

export const cssHomepageWrapper = css({
  padding: 20,
  '@media(min-width: 992px)': {
    padding: 50,
  },
});

export const cssDonasiWrapper = css({
  display: 'flex',
  flexWrap: 'wrap',
  flex: 1,
  alignItems: 'flex-start',
});

export const cssDonasiItemWrapper = css({
  boxSizing: 'border-box',
  alignSelf: 'flex-start',
  '@media(max-width: 768px)': {
    flex: '100%',
    paddingBottom: 20,
  },
  '@media(min-width: 768px)': {
    flex: `0 1 ${100 / 2}%`,
    padding: '0 20px 30px',
  },
  '@media(min-width: 1366px)': {
    flex: `0 1 ${100 / 4}%`,
  },
});

export const cssFilterWrapper = css({
  marginBottom: 50,
  width: '100%',
  alignItems: 'center',
  display: 'flex',
  boxSizing: 'border-box',
  '@media(max-width: 600px)': {
    flexDirection: 'column',
  },
  '@media(min-width: 600px)': {
    padding: '0 20px',
  },
});

export const cssMainIcon = css({
  '@media(max-width: 600px)': {
    marginTop: 10,
    marginBottom: 30,
  },
});

export const cssSelectFilter = css({
  padding: '12px 16px',
  '@media(max-width: 600px)': {
    width: '100%',
  },
});
