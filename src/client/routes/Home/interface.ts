export interface ApiBaseResponse<DT> {
  response_code: string;
  response_desc: Responsedesc;
  data: DT;
  hasNext: boolean;
}

interface Responsedesc {
  id: string;
  en: string;
}
