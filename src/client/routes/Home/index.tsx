import loadable from '@loadable/component';

const Home = loadable(() => import(/* webpackChunkName: "home-page" */ './view'), {
  ssr: true,
});

export default Home;
