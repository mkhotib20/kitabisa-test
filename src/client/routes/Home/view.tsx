import useAxios from '@/hooks/useAxios';
import { Donasi } from '@/types/donasi';
import { useCallback, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import DonasiList from './components/DonasiList';
import Filter from './components/Filter';
import { API_URL } from './const';
import { ApiBaseResponse } from './interface';
import { cssHomepageWrapper } from './styles';

const ViewHome = () => {
  const [page, setPage] = useState(1);
  const [filter, setFilter] = useState<Record<string, string>>({});

  const { data, loading, refetch, fetchMore } = useAxios<ApiBaseResponse<Donasi[]>>(API_URL);

  const handleNextPage = useCallback(async () => {
    const newPage = page + 1;
    await fetchMore({
      variables: {
        page: `${newPage}`,
        ...filter,
      },
      updateData: (prevResult, newResult) => ({
        ...newResult,
        data: [...prevResult.data, ...newResult.data],
      }),
    });
    setPage(newPage);
  }, [page, filter]);

  const handleFilter = async (variables: Record<string, string>) => {
    await refetch(variables);
    setFilter(variables);
    setPage(1);
  };

  return (
    <div css={cssHomepageWrapper}>
      <Helmet>
        <title>Kitabisa - Daftar Kampanye</title>
      </Helmet>
      <Filter onFilter={handleFilter} />
      <DonasiList loading={loading} onNextPage={handleNextPage} response={data} />
    </div>
  );
};

export default ViewHome;
