import renderWrapper from '@/utils/test/renderWrapper';
import '@testing-library/jest-dom';
import { screen } from '@testing-library/react';
import Routes from '../index';

jest.mock('axios');

describe('Routes Page', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('Should render correctly with error and show error', async () => {
    renderWrapper(<Routes />);
    expect(await screen.findByText(/Something went wrong, please try again later/i)).toBeInTheDocument();
  });
});
