import { HelmetProvider } from 'react-helmet-async';
import Routes from './routes';
import { globalStyles } from './styles/global';

const App = () => (
  <HelmetProvider>
    {globalStyles}
    <Routes />
  </HelmetProvider>
);

export default App;
