import axios from 'axios';
import { Request, Response } from 'express';

const JSON_DATA_SOURCE = process.env.JSON_DATA_SOURCE_URL;

/**
 * This function is aimed to mock backend so that the data can be shown based on order type
 * @param {Request} req Request object from express
 * @param {Response} res Response object from express
 */

const handleGetCampaign = async (req: Request, res: Response) => {
  const { order, page } = req.query;
  const pageNumber = parseInt(page?.toString(), 10) || 1;

  const { data } = await axios.get(JSON_DATA_SOURCE);
  if (order) {
    const [field, type] = order.toString().split(',');
    data.data = data.data.sort((left, right) => {
      if (type === 'desc') {
        return right[field] - left[field];
      } else if (type === 'asc') {
        return left[field] - right[field];
      }
    });
  }
  data.data = data.data.map((item) => ({
    ...item,
    id: item.id + (pageNumber - 1),
  }));

  res.json({
    ...data,
    // Mock paginations
    hasNext: pageNumber < 5,
  });
};

export default handleGetCampaign;
