import React from 'react';
import { StaticRouter } from 'react-router-dom/server';
import App from '../client/App';
import { ServerDataContext } from './context/ssrData';

const EntryPoint = ({ url }) => (
  <ServerDataContext>
    <React.StrictMode>
      <StaticRouter location={url}>
        <App />
      </StaticRouter>
    </React.StrictMode>
  </ServerDataContext>
);

export default EntryPoint;
