import { ChunkExtractor } from '@loadable/server';
import { load } from 'cheerio';
import path from 'path';
import ReactDOMServer from 'react-dom/server';
import { resolveData } from './context/ssrData';
import EntryPoint from './EntryPoint';

const renderWithLoadable = async (url) => {
  const statsFile = path.resolve('dist/client/loadable-stats.json');
  const extractor = new ChunkExtractor({ statsFile });
  const jsx = extractor.collectChunks(<EntryPoint url={url} />);

  ReactDOMServer.renderToString(jsx);

  const data = await resolveData();

  const html = ReactDOMServer.renderToString(jsx);
  const $ = load(html);

  let imgToPreload = '';
  $('img').each((_, element) => {
    const src = $(element).attr('src');
    imgToPreload += `<link rel="preload" href="${src}" as="image">`;
  });

  const scriptTags = extractor.getScriptTags();
  const linkTags = extractor.getLinkTags();
  const styleTags = extractor.getStyleTags();

  return {
    html,
    scriptTags,
    linkTags,
    styleTags,
    imgToPreload,
    initialProps: data.toHtml(),
  };
};

export default renderWithLoadable;
