import express from 'express';
import * as fs from 'fs';
import { compile } from 'handlebars';
import * as minifier from 'html-minifier';
import cors from 'cors';
import path from 'path';
import serveStatic from 'serve-static';
import handleGetCampaign from './api/get-campaign';
import renderWithLoadable from './loadbale-server';

const htmlTemplate = fs.readFileSync(path.join(__dirname, 'renderer/html-template.html')).toString();

const server = express();
server.use(
  '/',
  express.static(path.join(__dirname, '../client'), {
    maxAge: '1d',
    setHeaders: (res, path) => {
      if (serveStatic.mime.lookup(path) === 'text/html') {
        res.setHeader('Cache-Control', 'public, max-age=0');
      }
    },
  })
);
server.use(
  cors({
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204,
    origin: ['http://localhost:3000', 'https://kitabisa.rebase.my.id'],
  })
);

server.get('/api/get-campaign', handleGetCampaign);

server.get('*', async (req, res) => {
  const buildParam = await renderWithLoadable(req.url);

  const template = compile(htmlTemplate);
  const htmlWithData = template(buildParam);
  const minifiedHtml = minifier.minify(htmlWithData, {
    collapseWhitespace: true,
    keepClosingSlash: true,
  });

  res.type('html').send(minifiedHtml);
});

server.listen(process.env.PORT, () => {
  console.log(`Server running on http://localhost:${process.env.PORT}`);
});
