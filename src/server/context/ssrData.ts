import { createServerContext } from 'use-sse';

export const { ServerDataContext, resolveData } = createServerContext();
