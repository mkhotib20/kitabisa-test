module.exports = {
  moduleDirectories: ['<rootDir>/'],
  testEnvironment: 'jest-environment-jsdom',
  moduleNameMapper: {
    '@/(.*)$': '<rootDir>/src/client/$1',
    '\\.(png|svg)$': '<rootDir>/src/client/__mocks__/fileMock.ts',
  },
  coverageReporters: ['html'],
  setupFiles: ['dotenv/config'],
  collectCoverageFrom: [
    '<rootDir>/src/client/**/*.tsx',
    '<rootDir>/src/client/**/*.ts',
    '!<rootDir>/src/client/index.tsx',
    '!<rootDir>/src/client/App.tsx',
    '!<rootDir>/src/client/styles/*',
    '!<rootDir>/src/client/utils/test/*',
  ],
};
